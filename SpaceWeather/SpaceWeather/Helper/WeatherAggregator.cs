﻿using SpaceWeather.Models.Enums;
using SpaceWeather.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Helper
{
    public class WeatherAggregator
    {
        public List<WeatherAggregate> AggregateWeatherData(List<Weather> records)
        {
            var filteredRecords = records.Where(record =>
            record.Temperature >= 2 &&
            record.Temperature <= 31 &&
            record.Wind <= 10 &&
            record.Humidity < 60 &&
            record.Precipitation == 0 &&
            record.Lightning == false &&
            record.Clouds != CloudType.Cumulus &&
            record.Clouds != CloudType.Nimbus).ToList();

            var launchDay = filteredRecords.FirstOrDefault();

            if (launchDay == null)
            {
                return new List<WeatherAggregate>();
            }

            var aggregatedData = new List<WeatherAggregate>
            {
                new WeatherAggregate
                {
                    Parameter = "Temperature (C)",
                    AverageValue = Math.Round(records.Average(record => record.Temperature)),
                    MaxValue = records.Max(record => record.Temperature),
                    MinValue = records.Min(record => record.Temperature),
                    MedianValue = records.OrderBy(record => record.Temperature).ElementAt(filteredRecords.Count / 2).Temperature,
                    LaunchDayParameterValue = launchDay.Temperature
                },
                new WeatherAggregate
                {
                    Parameter = "Wind (m/s)",
                    AverageValue = Math.Round(records.Average(record => record.Wind)),
                    MaxValue = records.Max(record => record.Wind),
                    MinValue = records.Min(record => record.Wind),
                    MedianValue = records.OrderBy(record => record.Wind).ElementAt(filteredRecords.Count / 2).Wind,
                    LaunchDayParameterValue = launchDay.Wind
                },
                new WeatherAggregate
                {
                    Parameter = "Humidity (%)",
                    AverageValue = Math.Round(records.Average(record => record.Humidity)),
                    MaxValue = records.Max(record => record.Humidity),
                    MinValue = records.Min(record => record.Humidity),
                    MedianValue = records.OrderBy(record => record.Humidity).ElementAt(filteredRecords.Count / 2).Humidity,
                    LaunchDayParameterValue = launchDay.Humidity
                },
                new WeatherAggregate
                {
                    Parameter = "Precipitation (%)",
                    AverageValue = Math.Round(records.Average(record => record.Precipitation)),
                    MaxValue = records.Max(record => record.Precipitation),
                    MinValue = records.Min(record => record.Precipitation),
                    MedianValue = records.OrderBy(record => record.Precipitation).ElementAt(filteredRecords.Count / 2).Precipitation,
                    LaunchDayParameterValue = launchDay.Precipitation
                }
            };

            return aggregatedData;
        }
    }
}
