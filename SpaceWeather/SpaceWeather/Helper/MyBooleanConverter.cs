﻿using CsvHelper.Configuration;
using CsvHelper;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Helper
{
	public class MyBooleanConverter : DefaultTypeConverter
	{
		public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
		{
			if (value == null)
			{
				return string.Empty;
			}
			var boolValue = (bool)value;
			return boolValue ? "Yes" : "No";
		}

		public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
		{
			if (string.IsNullOrEmpty(text))
			{
				return false;
			}
			var normalizedText = text.ToLowerInvariant();
			switch (normalizedText)
			{
				case "yes":
				case "true":
				case "1":
					return true;
				case "no":
				case "false":
				case "0":
					return false;
				default:
					throw new InvalidOperationException();
			}
		}
	}
}
