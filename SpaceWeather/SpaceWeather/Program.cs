﻿using Autofac;
using CsvHelper;
using SpaceWeather.Models;
using SpaceWeather.Services;
using SpaceWeather.Services.Contracts;
using System;
using System.Globalization;
using System.IO;

namespace SpaceWeather
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var container = ConfigureContainer();

			var application = container.Resolve<ApplicationService>();

			application.Run();
		}

		private static IContainer ConfigureContainer()
		{
			var builder = new ContainerBuilder();

			builder.RegisterType<ApplicationService>().AsSelf();
			builder.RegisterType<UserMenuService>().As<IUserMenuService>();
			builder.RegisterType<EmailSender>().As<IEmailSender>();

			return builder.Build();
		}
	}
}
