﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Constants
{
    public class ExceptionMessages
    {
        public const string IvalidCredentials = "Incorrect email or password.";
        public const string InvalidEmailFormat = "Incorrect email format.";
        public const string InvalidDirectory = "File directory not found.";
        public const string NullObject = "Input cannot be null!";
    }
}
