﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Models.Enums
{
	public enum CloudType
	{
		Cumulus,
		Stratus,
		Nimbus,
		Cirrus
	}
}
