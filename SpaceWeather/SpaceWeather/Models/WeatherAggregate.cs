﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Models
{
    public class WeatherAggregate
    {
        public string Parameter { get; set; }
        public double AverageValue { get; set; }
        public int MaxValue { get; set; }
        public int MinValue { get; set; }
        public int MedianValue { get; set; }
        public int LaunchDayParameterValue { get; set; }
    }

    /// <summary>
    /// Configuration for the WeatherAggregate model.
    /// </summary>
    public class WeatherAggregateClassMap : ClassMap<WeatherAggregate>
    {
        public WeatherAggregateClassMap()
        {
            AutoMap(CultureInfo.InvariantCulture);

            Map(m => m.AverageValue).Name("Average Value");
            Map(m => m.MaxValue).Name("Max Value");
            Map(m => m.MinValue).Name("Min Value");
            Map(m => m.MedianValue).Name("Median Value");
            Map(m => m.LaunchDayParameterValue).Name("Launch Day Parameter Value");
        }
    }
}
