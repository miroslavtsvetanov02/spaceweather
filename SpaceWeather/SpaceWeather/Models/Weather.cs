﻿using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using CsvHelper.TypeConversion;
using SpaceWeather.Helper;
using SpaceWeather.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Models
{
	public class Weather
	{
		public int Day { get; set; }

		public int Temperature { get; set; }

		public int Wind { get; set; }

		public int Humidity { get; set; }

		public int Precipitation { get; set; }

		public bool Lightning { get; set; }

		public CloudType Clouds { get; set; }

		/// <summary>
		/// Seed method for creating the data needed for the report.
		/// </summary>
		/// <returns>A list of Weather objects.</returns>
		public static List<Weather> CreateWeather()
		{
			return new List<Weather>
			{
				new Weather
				{
					Day = 1,
					Temperature = 28,
					Wind = 15,
					Humidity = 20,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cumulus,
				},

				new Weather
				{
					Day = 2,
					Temperature = 28,
					Wind = 13,
					Humidity = 30,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cumulus,
				},
				new Weather
				{
					Day = 3,
					Temperature = 29,
					Wind = 12,
					Humidity = 30,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Stratus,
				},
				new Weather
				{
					Day = 4,
					Temperature = 30,
					Wind = 14,
					Humidity = 35,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Stratus,
				},
				new Weather
				{
					Day = 5,
					Temperature = 31,
					Wind = 11,
					Humidity = 60,
					Precipitation = 20,
					Lightning = false,
					Clouds = CloudType.Stratus,
				},
				new Weather
				{
					Day = 6,
					Temperature = 32,
					Wind = 10,
					Humidity = 70,
					Precipitation = 40,
					Lightning = true,
					Clouds = CloudType.Nimbus,
				},
				new Weather
				{
					Day = 7,
					Temperature = 31,
					Wind = 6,
					Humidity = 80,
					Precipitation = 30,
					Lightning = true,
					Clouds = CloudType.Nimbus,
				},
				new Weather
				{
					Day = 8,
					Temperature = 30,
					Wind = 5,
					Humidity = 60,
					Precipitation = 20,
					Lightning = false,
					Clouds = CloudType.Stratus,
				},
				new Weather
				{
					Day = 9,
					Temperature = 28,
					Wind = 4,
					Humidity = 30,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cumulus,
				},
				new Weather
				{
					Day = 10,
					Temperature = 28,
					Wind = 3,
					Humidity = 20,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cirrus,
				},
				new Weather
				{
					Day = 11,
					Temperature = 27,
					Wind = 2,
					Humidity = 25,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cumulus,
				},
				new Weather
				{
					Day = 12,
					Temperature = 29,
					Wind = 3,
					Humidity = 20,
					Precipitation = 5,
					Lightning = false,
					Clouds = CloudType.Stratus,
				},
				new Weather
				{
					Day = 13,
					Temperature = 31,
					Wind = 2,
					Humidity = 15,
					Precipitation = 5,
					Lightning = false,
					Clouds = CloudType.Cirrus,
				},
				new Weather
				{
					Day = 14,
					Temperature = 32,
					Wind = 2,
					Humidity = 15,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cirrus,
				},
				new Weather
				{
					Day = 15,
					Temperature = 32,
					Wind = 2,
					Humidity = 20,
					Precipitation = 0,
					Lightning = false,
					Clouds = CloudType.Cirrus,
				},
			};
		}
	}

	/// <summary>
	/// Configuration for the Weather model.
	/// </summary>
	public class WeatherClassMap : ClassMap<Weather>
	{
		public WeatherClassMap()
		{
			Map(m => m.Day).Name("Day");
			Map(m => m.Temperature).Name("Temperature (C)");
			Map(m => m.Wind).Name("Wind (m/s)");
			Map(m => m.Humidity).Name("Humidity (%)");
			Map(m => m.Precipitation).Name("Precipitation (%)");
            Map(m => m.Lightning).Name("Lightning").TypeConverter<MyBooleanConverter>();
			Map(m => m.Clouds).Name("Clouds"); ;
		}
	}
}
