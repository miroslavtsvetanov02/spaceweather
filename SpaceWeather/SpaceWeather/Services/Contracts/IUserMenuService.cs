﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Services.Contracts
{
	public interface IUserMenuService
	{
		void CalculateBestDay(string path, string senderEmail, string password, string receiverEmail);

		void GenerateDataCsvFile();

		string CreateSeedFolder();

    }
}
