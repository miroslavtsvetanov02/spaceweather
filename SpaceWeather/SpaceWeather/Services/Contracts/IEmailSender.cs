﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceWeather.Services.Contracts
{
	public interface IEmailSender
	{
		void SendEmailWithAttachment(string attachmentFilePath, string from, string password, string to);
	}
}
