﻿using SpaceWeather.Services.Contracts;
using System;
using System.Net.Mail;

namespace SpaceWeather.Services
{
	public class ApplicationService : IApplicationService
	{

		private readonly IUserMenuService _userMenuService;

		public ApplicationService(IUserMenuService userMenuService)
		{
			_userMenuService = userMenuService;
		}

		public void Run()
		{
            DisplayMenu();
		}

        #region Main User Menu
        /// <summary>
        /// The display menu for a user and his options.
        /// </summary>
        public void DisplayMenu()
		{
			bool done = false;

			while (!done)
			{
                Console.WriteLine("Main Menu\n" + "---------");
                Console.WriteLine("1. Calculate the best day for SPACE launch");
                Console.WriteLine("2. Exit");

                var userChoice = Console.ReadLine();

                Console.Clear();

                try
                {
                    switch (userChoice)
                    {
                        case "1":

                            Console.Write("Enter the save path: ");
                            string path = Console.ReadLine();

                            Console.Write("\nEnter your email(outlook only): ");
                            string emailSender = Console.ReadLine();

                            Console.Write("\nEnter your password: ");
                            string password = Console.ReadLine();

                            Console.Write("\nEnter the email which will receive the report: ");
                            string emailReceiver = Console.ReadLine();

                            _userMenuService.GenerateDataCsvFile();
                            _userMenuService.CalculateBestDay(path, emailSender, password, emailReceiver);

                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write("Done!");
                            Console.ResetColor();

                            done = true;
                            break;

                        case "2":
                            done = true;
                            break;

                        default:
                            continue;
                    }
                }
                catch (Exception)
                {
                    DisplayMenu();
                    break;
                }
            }
		}
        #endregion
    }
}
