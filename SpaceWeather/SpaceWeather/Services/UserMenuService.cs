﻿using CsvHelper;
using SpaceWeather.Models;
using SpaceWeather.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net;
using SpaceWeather.Constants;
using SpaceWeather.Models.Enums;
using SpaceWeather.Helper;

namespace SpaceWeather.Services
{
	public class UserMenuService : IUserMenuService
	{
		private readonly IEmailSender _emailService;

		public UserMenuService(IEmailSender emailService)
		{
			_emailService = emailService;
		}

        #region Calculate Best Day
        /// <summary>
        /// This method calculates the best day for space launch based on the
        /// Weather report which is created first and then checks if a report with the
        /// same name is created it automatically goes to SendEmailWithAttachment method.
        /// </summary>
        /// <param name="path">The file path of the weather report file.</param>
        /// <param name="senderEmail">The email of the sender.</param>
        /// <param name="password">The password of the email sender</param>
        /// <param name="receiverEmail">The email of the receiver.</param>
        /// <exception cref="Exception"></exception>
        public void CalculateBestDay(string path, string senderEmail, string password, string receiverEmail)
		{
            try
            {
                var seedFolder = CreateSeedFolder();
                var subFolderPath = Path.Combine(seedFolder, $"Weather.csv");

                var csvSavePath = Path.Combine(path, $"WeatherReport.csv");

                if (!File.Exists(csvSavePath))
                { 
                    var records = new List<Weather>();

                    using (var streamReader = new StreamReader(subFolderPath))
                    {
                        using (var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture))
                        {
                            csvReader.Context.RegisterClassMap<WeatherClassMap>();
                            records = csvReader.GetRecords<Weather>().ToList();
                        }
                    }

                    var aggregator = new WeatherAggregator();
                    var aggregatedData = aggregator.AggregateWeatherData(records);

                    using (var streamWriter = new StreamWriter(csvSavePath))
                    {
                        using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
                        {
                            csvWriter.Context.RegisterClassMap<WeatherAggregateClassMap>();

                            csvWriter.WriteRecords(aggregatedData);
                        }
                    }
                }

                _emailService.SendEmailWithAttachment(csvSavePath, senderEmail, password, receiverEmail);
            }

            catch (DirectoryNotFoundException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ExceptionMessages.InvalidDirectory + "\n");
                Console.ResetColor();

                throw new Exception();
            }
            catch (ArgumentException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ExceptionMessages.NullObject + "\n");
                Console.ResetColor();

                throw new Exception();
            }
        }
        #endregion

        #region Generate Main Report
        /// <summary>
        /// Generates the main Weather report with the data needed for the best SPACE launch day.
        /// </summary>
        public void GenerateDataCsvFile()
		{
            var subFolder = CreateSeedFolder();
            var csvPath = Path.Combine(subFolder, $"Weather.csv");

            if (!File.Exists(csvPath))
            {
                using (var streamWriter = new StreamWriter(csvPath))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
                    {
                        csvWriter.Context.RegisterClassMap<WeatherClassMap>();

                        var weather = Weather.CreateWeather();
                        csvWriter.WriteRecords(weather);
                    }
                }
            }
		}
        #endregion

        #region Create Seed Folder
        /// <summary>
        /// Check if the Seed folder exists and if it doesn't creates
        /// one with that name.
        /// </summary>
        /// <returns>Path to the seed folder.</returns>
        public string CreateSeedFolder()
        {
            var solutionDir = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            var subFolder = "Seed";
            var subFolderPath = Path.Combine(solutionDir, subFolder);

            if (!Directory.Exists(subFolderPath))
            {
                Directory.CreateDirectory(subFolderPath);
            }

            return subFolderPath;
        }
        #endregion
    }
}
