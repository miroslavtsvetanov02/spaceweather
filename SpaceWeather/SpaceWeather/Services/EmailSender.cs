﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SpaceWeather.Services.Contracts;
using SpaceWeather.Constants;

namespace SpaceWeather.Services
{
	public class EmailSender : IEmailSender
	{
        #region Email Sender
		/// <summary>
		/// This email sender uses the outlook server to send emails.
		/// </summary>
		/// <param name="attachmentFilePath">The file path of the weather report file.</param>
		/// <param name="from">The email of the sender.</param>
		/// <param name="password">The password of the email sender</param>
		/// <param name="to">The email of the receiver.</param>
		/// <exception cref="Exception"></exception>
        public void SendEmailWithAttachment(string attachmentFilePath, string from, string password, string to)
		{
			try
			{
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

				SmtpClient client = new SmtpClient("smtp-mail.outlook.com");

				client.Port = 587;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.UseDefaultCredentials = false;

				NetworkCredential credential =
					new NetworkCredential(from, password);

				client.EnableSsl = true;
				client.Credentials = credential;

				MailMessage message = new MailMessage(from, to);
				message.Subject = "Weather Report";
				message.Body = "This is the day when you can take the SPACE jump!";
				message.IsBodyHtml = false;

				Attachment attachment = new Attachment(attachmentFilePath);
				message.Attachments.Add(attachment);

				client.Send(message);

				attachment.Dispose();
				message.Dispose();
			}

			catch (FormatException)
			{
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ExceptionMessages.InvalidEmailFormat + "\n");
				Console.ResetColor();

                throw new Exception();
            }

			catch (SmtpException)
			{
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ExceptionMessages.IvalidCredentials + "\n");
				Console.ResetColor();

                throw new Exception();
            }
		}
        #endregion
    }
}
