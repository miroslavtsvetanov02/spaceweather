# SpaceWeather

A simple console program which generates weather report which is used to calculate the best day for a SPACE launch!

# Technologies

- Entity Framework Core
- CSV Helper
- Autofac
